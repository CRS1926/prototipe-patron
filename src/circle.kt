

class Circle: shape {

    constructor():super()

    constructor(target: Circle): super(target){
        if(target != null){
            this.radius = target.radius;
        }
    }

    public lateinit var radius: Integer

    override fun clone(): shape {
        return Circle(this)
    }

    fun areEquals(objeto2: Any): Boolean{
        if(!(objeto2 is Circle) || !super.areEqueals(objeto2)){
            return false
        }
        var shape2 = objeto2 as Circle
        return shape2.radius == this.radius
    }

}