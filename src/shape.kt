import kotlin.reflect.jvm.internal.impl.load.kotlin.JvmType
import kotlin.reflect.jvm.internal.impl.storage.NullableLazyValue


abstract class shape (){


    constructor(target: shape) : this(){
        if (target != null){
            this.x = target.x
            this.y = target.y
            this.color = target.color
        }
    }

    public lateinit var x: Integer
    public lateinit var y: Integer
    public lateinit var color: String


    public abstract fun clone(): shape

    public fun areEqueals(object2: Any): Boolean {
        if(!(object2 is shape)){
            return false
        }
        var shape2 = object2 as shape
        return shape2.x == this.x && shape2.y == this.y && shape2.color.equals(this.color)
    }


}