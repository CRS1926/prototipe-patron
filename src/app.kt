

fun cloneAndCompare(shapes: ArrayList<shape>, shapesCopy: ArrayList<shape>): Unit{

    for(shape: shape in shapes){
        shapesCopy.add(shape.clone())
    }

    for( i in 0 until shapes.size){
        if(shapes.get(i) != shapesCopy.get(i)){
            println("$i Sapes are different objects (yay!)")
            if(shapes.get(i).equals(shapes.get(i))){
                println("$i And they are identical (yay!)")
            } else {
                println("But they are not identical (boo !)")
            }
        } else {
            println("$i shape objects are the same (boo!)")
        }
    }
}


fun main(){
    println("------------------Sarted-------------")

    var shapes = ArrayList<shape>()
    var shapesCopy = ArrayList<shape>()

    var circle1 = Circle()

    circle1.x = 10 as Integer
    circle1.y = 20 as Integer
    circle1.radius = 15 as Integer
    circle1.color = "Red"
    shapes.add(circle1);

    var anotherCircle = circle1.clone() as Circle
    shapes.add(anotherCircle)

    var rectangle1 = Rectangle()
    rectangle1.x = 10 as Integer
    rectangle1.y = 5 as Integer
    rectangle1.width = 10 as Integer
    rectangle1.height = 20 as Integer
    rectangle1.color = "blue"
    shapes.add(rectangle1)

    cloneAndCompare(shapes,shapesCopy)

}

