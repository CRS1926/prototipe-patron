class Rectangle: shape {

    constructor(): super()

    constructor(target: Rectangle): super(target){
        if(target != null){
            this.width = target.width
            this.height = target.height
        }
    }

    public lateinit var width: Integer
    public lateinit var height: Integer


    override fun clone(): shape {
        return Rectangle(this)
    }

    fun areEquals(object2: Any):Boolean{
        if(object2 !is Rectangle || !(super.areEqueals(object2))){
            return false;
        }
        var shape2 = object2 as Rectangle
        return shape2.width == this.width && shape2.height == this.height
    }

}